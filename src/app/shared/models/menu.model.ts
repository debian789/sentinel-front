export class MenuModel {
    label: string;
    icon: string;
    url: string;
    children:[ChildrenMenu]

}

export class ChildrenMenu {
    label: string;
    icon: string;
    url: string;    
}


export interface Cliente {
    id: number;
    nombre: string;
    cif: string;
    direccion: string;
    grupo: number;
}

export interface Grupo {
    id: number;
    nombre: string;
}