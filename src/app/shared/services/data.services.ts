import {HttpClient, HttpHeaders} from '@angular/common/http'
import {Observable, of} from 'rxjs';
import {MenuModel} from '../models/menu.model';
import {catchError, map, tap} from 'rxjs/operators'
import {Injectable} from '@angular/core';

const httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
};
@Injectable({providedIn: 'root'})
export class DataService {
    private serviceUrl = 'http://localhost:3010/'

    constructor(private http : HttpClient) {}

    getMenu() : Observable < MenuModel[] > {
        return this.http.get < MenuModel[] > (this.serviceUrl + "menu").pipe(tap(_ => null , catchError(this.handleError('get menu', []))))
    }

    /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
    private handleError < T > (operation = 'operation', result?: T) {
        return(error : any): Observable < T > => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

}
