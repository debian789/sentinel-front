import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/shared/services/data.services';
import { MenuModel } from 'src/app/shared/models/menu.model';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.styl']
})
export class MenuComponent implements OnInit {

  menu: MenuModel[];

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.getMenu().subscribe(menuItem => { 
      
      this.menu = menuItem as MenuModel[]})
  }

}
