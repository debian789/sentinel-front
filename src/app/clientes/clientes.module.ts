import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ClientesService} from './clientes.service';
import { AltaClienteComponent } from './alta-cliente/alta-cliente.component'

@NgModule({
  // Permite implementar los componentes 
  declarations: [AltaClienteComponent],
  imports: [
    CommonModule
    
  ],
  // Permite asociar lo servicios 
  providers: [
    ClientesService
  ],
  // Permite exportar componentes que se definieron en el modulo
  exports: [
    AltaClienteComponent
  ]
})
export class ClientesModule { }
