import { Component, OnInit } from '@angular/core';
import {ClientesService} from '../clientes.service'
import {Cliente, Grupo} from '../../shared/models/menu.model'

@Component({
  selector: 'app-alta-cliente',
  templateUrl: './alta-cliente.component.html',
  styleUrls: ['./alta-cliente.component.styl']
})
export class AltaClienteComponent implements OnInit {

  cliente: Cliente;
  grupo: Grupo[]

  constructor(private clientesService: ClientesService) { }

  ngOnInit() { 
    this.cliente = this.clientesService.nuevoCliente();
    this.grupo = this.clientesService.getGrupos();
  }

  nuevoCliente(): void {
    this.clientesService.agregarCliente(this.cliente)
    this.cliente = this.clientesService.nuevoCliente();

  }

}
